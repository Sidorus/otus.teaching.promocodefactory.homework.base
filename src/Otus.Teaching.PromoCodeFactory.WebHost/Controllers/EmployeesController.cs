﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Employees
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEntityService<Employee> _employeeService;
        private readonly IMapper _mapper;

        public EmployeesController(IEntityService<Employee> employeeService, IMapper mapper)
        {
            _employeeService = employeeService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get all employees
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeService.GetAllAsync();
            return _mapper.Map<List<EmployeeShortResponse>>(employees);
        }

        /// <summary>
        /// Get employee data by Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("get/{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeService.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            return _mapper.Map<EmployeeResponse>(employee);
        }

        /// <summary>
        /// Create employee
        /// </summary>
        /// <returns></returns>
        [HttpPost("create")]
        public async Task<EmployeeResponse> CreateEmployeeAsync(EmployeeRequest employeeRequest)
        {
            var employee = _mapper.Map<Employee>(employeeRequest);
            await _employeeService.CreateAsync(employee);
            return _mapper.Map<EmployeeResponse>(employee);
        }

        /// <summary>
        /// Update employee
        /// </summary>
        /// <returns></returns>
        [HttpPut("update/{id:guid}")]
        public async Task<EmployeeResponse> UpdateEmployeeAsync(Guid id, EmployeeRequest employeeRequest)
        {
            var employee = _mapper.Map<Employee>(employeeRequest);
            employee.Id = id;
            await _employeeService.UpdateAsync(employee);
            return _mapper.Map<EmployeeResponse>(employee);
        }

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <returns></returns>
        [HttpDelete("delete/{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            await _employeeService.DeleteAsync(id);
            return Ok();
        }
    }
}