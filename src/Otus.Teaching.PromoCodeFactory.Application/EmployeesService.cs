﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Application
{
    public class EmployeesService : IEntityService<Employee> 
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IEntityService<Role> _roleService;
        public EmployeesService(IRepository<Employee> employeeRepository, IEntityService<Role> roleService)
        {
            _employeeRepository = employeeRepository;
            _roleService = roleService;
        }
        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
            return await _employeeRepository.GetAllAsync();
        }

        public async Task<Employee> GetByIdAsync(Guid id)
        {
            return await _employeeRepository.GetByIdAsync(id);
        }

        public async Task<Employee> CreateAsync(Employee entity)
        {
            _ = entity ?? throw new ArgumentNullException(nameof(entity));
            entity.Id = Guid.NewGuid();
            var entityRoles = await UpdateRolesAsync(entity);
            entity.Roles = entityRoles;
            return await _employeeRepository.CreateAsync(entity);

        }

        public async Task<Employee> UpdateAsync(Employee entity)
        {
            _ = entity ?? throw new ArgumentNullException(nameof(entity));
            var entityRoles = await UpdateRolesAsync(entity);
            entity.Roles = entityRoles;
            return await _employeeRepository.UpdateAsync(entity);
        }

        public async Task DeleteAsync(Guid id)
        {
            await _employeeRepository.DeleteAsync(id);
        }

        private async Task<List<Role>> UpdateRolesAsync(Employee entity)
        {
            _ = entity ?? throw new ArgumentNullException(nameof(entity));
            var roles = await _roleService.GetAllAsync();
            var entityRoles = new List<Role>();
            foreach (var roleName in entity.Roles.Where(p => p.Id == Guid.Empty))
            {
                var role = roles.FirstOrDefault(r => r.Name == roleName.Name);
                if (role != null)
                {
                    entityRoles.Add(role);
                }
            }
            return entityRoles;
        }
    }
}
