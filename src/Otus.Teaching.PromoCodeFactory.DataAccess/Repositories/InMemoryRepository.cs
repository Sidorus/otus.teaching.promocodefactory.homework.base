﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T entity)
        {
            Data.Add(entity);
            return Task.FromResult(entity);
        }

        public async Task<T> UpdateAsync(T entity)
        {
            var dataItem = await GetByIdAsync(entity.Id);
            _ = dataItem ?? throw new KeyNotFoundException(nameof(dataItem));
            Data[Data.IndexOf(dataItem)] = entity;
            return await Task.FromResult(entity);
        }

        public async Task DeleteAsync(Guid id)
        {
            var dataItem = await GetByIdAsync(id);
            _ = dataItem ?? throw new KeyNotFoundException(nameof(dataItem));
            Data.Remove(dataItem);
        }
    }
}